import { gql } from '@apollo/client';

export const GET_ALL_FABRICS = gql`
     {
          fabrics {
               id
               name
               gsm
               length
               weight
               widthType
          }
     }
`;

export const CREATE_FABRIC = gql`
     mutation (
          $name: String!
          $gsm: Float!
          $length: Float!
          $weight: Float!
          $widthType: String!
     ) {
          createFabric(
               data: {
                    name: $name
                    gsm: $gsm
                    length: $length
                    weight: $weight
                    widthType: $widthType
               }
          ) {
               id
               name
               gsm
               length
               weight
               widthType
          }
     }
`;
