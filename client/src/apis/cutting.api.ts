import { gql } from '@apollo/client';

export const GET_ALL_CUTTINGS = gql`
     {
          cuttings {
               id
               layerLength
               totalWeight
               layerWeight
               date
          }
          w
     }
`;

export const CREATE_CUTTING = gql`
     mutation (
          $layerLength: Float!
          $totalWeight: Float!
          $layerWeight: Float!
          $date:String!
     ) {
          createFabric(
               data: {
                    name: $name
                    gsm: $gsm
                    length: $length
                    weight: $weight
                    widthType: $widthType
               }
          ) {
               id
               name
               gsm
               length
               weight
               widthType
          }
     }
`;
