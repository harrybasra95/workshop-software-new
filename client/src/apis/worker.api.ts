import { gql } from '@apollo/client';

export const GET_ALL_WORKERS = gql`
     {
          workers {
               id
               name
          }
     }
`;

export const CREATE_WORKER = gql`
     mutation ($name: String!) {
          createWorker(data: { name: $name }) {
               name
               id
          }
     }
`;
