import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import ReactRouter from './router';

const client = new ApolloClient({
     uri: 'http://localhost:4000/graphql',
     cache: new InMemoryCache(),
});

ReactDOM.render(
     <React.StrictMode>
          <ApolloProvider client={client}>
               <ReactRouter />
          </ApolloProvider>
     </React.StrictMode>,
     document.getElementById('root')
);
