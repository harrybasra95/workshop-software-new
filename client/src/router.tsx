import Cutting from 'pages/Cutting';
import Fabric from 'pages/Fabric';
import Worker from 'pages/Worker';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Dashboard from './pages/Dashboard';

const ReactRouter = () => {
     return (
          <Router>
               <Switch>
                    <Route path="/" exact component={Dashboard} />
                    <Route path="/workers" exact component={Worker} />
                    <Route path="/fabrics" exact component={Fabric} />
                    <Route path="/cuttings" exact component={Cutting} />
               </Switch>
          </Router>
     );
};

export default ReactRouter;
