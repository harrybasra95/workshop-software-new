import PlusIcon from 'components/PlusIcon';
import SidebarComponent from 'components/Sidebar';
import React from 'react';

const OuterLayout: React.FunctionComponent<{ plusIconClick?: Function }> = (
     props
) => {
     return (
          <div>
               <SidebarComponent />
               <div className="pl-64 pt-5">{props.children}</div>
               <PlusIcon plusIconClick={props.plusIconClick} />
          </div>
     );
};

export default OuterLayout;
