import { CreateCuttingSizeDTO, CuttingSizeI } from './cuttingSize.interface';

export interface CreateCuttingDTO {
     date: string;
     fabricId: string;
     layerLength: number;
     layerWeight: number;
     totalWeight: number;
     cuttingSizes: [CreateCuttingSizeDTO];
}

export interface CuttingI {
     id: string;
     date: string;
     fabric: string;
     fabricId: string;
     layerLength: number;
     layerWeight: number;
     totalWeight: number;
     cuttingSizes: [CuttingSizeI];
}
