export interface CreateWorkerDTO {
     name: string;
}

export interface WorkerI {
     name: string;
     id: string;
}
