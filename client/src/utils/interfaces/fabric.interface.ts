export interface CreateFabricDTO {
     name: string;
     widthType: 'open' | 'tube';
     gsm: number;
     weight: number;
     length: number;
}

export interface FabricI {
     name: string;
     id: string;
     widthType: 'open' | 'tube';
     gsm: number;
     weight: number;
     length: number;
}
