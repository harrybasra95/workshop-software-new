export interface CreateCuttingSizeDTO {
     size: number;
     quantity: number;
}

export interface CuttingSizeI {
     id: string;
     size: number;
     quantity: number;
}
