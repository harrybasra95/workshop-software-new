import { Link } from 'react-router-dom';

const SidebarComponent = () => {
     return (
          <div>
               <div className="fixed h-full w-60 bg-blue-400 left bottom p-2.5 pt-5">
                    <div className="text-center mb-6">
                         <p className="text-xl text-white ">Software</p>
                    </div>
                    <div className="">
                         <ul>
                              <li>
                                   <Link to="/workers">Workers</Link>
                              </li>
                              <li>
                                   <Link to="/fabrics">Fabrics</Link>
                              </li>{' '}
                              <li>
                                   <Link to="/cuttings">Cuttings</Link>
                              </li>
                         </ul>
                    </div>
               </div>
          </div>
     );
};

export default SidebarComponent;
