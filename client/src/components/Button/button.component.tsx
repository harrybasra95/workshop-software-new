const Button: React.FunctionComponent<{
     onClick?: Function;
     buttonText: string;
}> = ({ onClick, buttonText }) => {
     return (
          <div className="flex align-middle justify-center">
               <button
                    className="bg-blue-500 p-1 px-4 m-4"
                    onClick={() => (onClick ? onClick() : null)}
               >
                    {buttonText}
               </button>
          </div>
     );
};

export default Button;
