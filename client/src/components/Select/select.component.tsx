const Select: React.FunctionComponent<{
     label: string;
     value: any;
     options: Array<{ name: string; value: string }>;
     onChange: Function;
}> = ({ value, label, onChange, options }) => {
     return (
          <div className="flex  justify-between px-2 mb-1">
               <p className="mr-2 align-middle">{label}</p>
               <select onChange={({ target: { value } }) => onChange(value)}>
                    {options.map(({ name, value }) => (
                         <option value={value}>{name}</option>
                    ))}
               </select>
          </div>
     );
};

export default Select;
