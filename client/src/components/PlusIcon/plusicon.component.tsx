const PlusIcon: React.FunctionComponent<{
     plusIconClick?: Function;
}> = ({ plusIconClick }) => {
     return (
          <div>
               <div className="fixed right-10 bottom-10">
                    <div
                         onClick={() =>
                              plusIconClick ? plusIconClick() : null
                         }
                         className="h-14 w-14 bg-blue-500 rounded-full flex justify-center items-center"
                    >
                         <p className="text-5xl text-white ">+</p>
                    </div>
               </div>
          </div>
     );
};

export default PlusIcon;
