import { CrossIcon } from 'icons/cross.icon';

const PopUp: React.FunctionComponent<{
     title: string;
     isVisible: boolean;
     hideBoxFunction: Function;
}> = (props) => {
     return (
          <div>
               {props.isVisible ? (
                    <div className=" fixed h-full w-full bg-black bg-opacity-50  top-0 left-0 ">
                         <div className="transform h-2/4 w-1/4  bg-white relative overflow-auto top-2/4 left-2/4  -translate-x-2/4 -translate-y-2/4 ">
                              <div
                                   onClick={() => props.hideBoxFunction()}
                                   className="cross-icon absolute top-1 right-1 h-7 w-7 "
                              >
                                   <CrossIcon />
                              </div>
                              <div className="title text-center">
                                   <p className="text-2xl">{props.title}</p>
                              </div>
                              <div>{props.children}</div>
                         </div>
                    </div>
               ) : null}
          </div>
     );
};

export default PopUp;
