const Input: React.FunctionComponent<{
     onClick?: Function;
     label: string;
     value: any;
     type?: any;
     onChange: Function;
}> = ({ value, label, onChange, type }) => {
     return (
          <div className="flex  justify-between px-2 mb-1">
               <p className="mr-2 align-middle">{label}</p>
               <input
                    className="border-solid border-grey border p-2"
                    type={type || 'text'}
                    value={value}
                    onChange={({ target: { value } }) => onChange(value)}
               />
          </div>
     );
};

export default Input;
