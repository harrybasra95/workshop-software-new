import { useMutation, useQuery } from '@apollo/client';
import { GET_ALL_CUTTINGS, CREATE_CUTTING } from 'apis/cutting.api';
import { GET_ALL_FABRICS } from 'apis/fabric.api';
import Button from 'components/Button';
import Input from 'components/Input';
import PopUp from 'components/PopUp';
import Select from 'components/Select';
import OuterLayout from 'layouts/Outer';
import { useState } from 'react';
import { sizeArray } from 'utils/data/sizeArray';
import { CreateCuttingDTO, CuttingI } from 'utils/interfaces/cutting.interface';
import { FabricI } from 'utils/interfaces/fabric.interface';

export const Cutting = () => {
     const [isPopUpVisible, setisPopUpVisible] = useState(false);
     const { data } = useQuery<{ cuttings: CuttingI[] }>(GET_ALL_CUTTINGS);
     return (
          <OuterLayout
               plusIconClick={() => {
                    setisPopUpVisible(true);
               }}
          >
               <div>
                    <CuttingPopUp
                         isVisible={isPopUpVisible}
                         hideBoxFunction={() => {
                              setisPopUpVisible(false);
                         }}
                    />
                    <h1 className="text-2xl">Cuttings</h1>
                    <div>
                         <table className="table-auto mt-5">
                              <thead>
                                   <th className="border p-2 border-black">
                                        Date
                                   </th>
                                   <th className="border p-2 border-black">
                                        Layer Weight
                                   </th>
                                   <th className="border p-2 border-black">
                                        Layer Length
                                   </th>
                                   <th className="border p-2 border-black">
                                        Total Weight
                                   </th>
                              </thead>
                              <tbody>
                                   {data &&
                                        data.cuttings.map(
                                             ({
                                                  date,
                                                  layerWeight,
                                                  layerLength,
                                                  totalWeight,
                                             }) => (
                                                  <tr>
                                                       <td className="p-2 border border-black">
                                                            {date}
                                                       </td>
                                                       <td className="p-2 border border-black">
                                                            {layerWeight}
                                                       </td>
                                                       <td className="p-2 border border-black">
                                                            {layerLength}
                                                       </td>
                                                       <td className="p-2 border border-black">
                                                            {totalWeight}
                                                       </td>
                                                  </tr>
                                             )
                                        )}
                              </tbody>
                         </table>
                    </div>
               </div>
          </OuterLayout>
     );
};

export const CuttingPopUp: React.FunctionComponent<{
     isVisible: boolean;
     hideBoxFunction: Function;
}> = ({ isVisible, hideBoxFunction }) => {
     const [createCutting] = useMutation(CREATE_CUTTING);
     const { data } = useQuery<{ fabrics: FabricI[] }>(GET_ALL_FABRICS);
     const [cuttingData, setCuttingData] = useState<CreateCuttingDTO>({
          date: '',
          fabricId: data ? data.fabrics[0].id : '',
          layerLength: 0,
          layerWeight: 0,
          totalWeight: 0,
          cuttingSizes: [
               {
                    quantity: 0,
                    size: 0,
               },
          ],
     });
     console.log(cuttingData);
     return (
          <PopUp
               title="Add Cutting"
               isVisible={isVisible}
               hideBoxFunction={hideBoxFunction}
          >
               <div className="flex flex-col align-middle justify-center pt-5">
                    <Select
                         label="Fabric"
                         value={cuttingData.fabricId}
                         onChange={(value: any) =>
                              setCuttingData({
                                   ...cuttingData,
                                   fabricId: value,
                              })
                         }
                         options={
                              data?.fabrics.map((e) => ({
                                   name: e.name,
                                   value: e.id,
                              })) || []
                         }
                    />
                    <Input
                         label="Date"
                         type="date"
                         value={cuttingData.date}
                         onChange={(value: string) =>
                              setCuttingData({ ...cuttingData, date: value })
                         }
                    />

                    <Input
                         label="Layer Length"
                         value={cuttingData.layerLength}
                         onChange={(value: string) =>
                              setCuttingData({
                                   ...cuttingData,
                                   layerLength: +value,
                              })
                         }
                    />
                    <Input
                         label="Layer Weight"
                         value={cuttingData.layerWeight}
                         onChange={(value: string) =>
                              setCuttingData({
                                   ...cuttingData,
                                   layerWeight: +value,
                              })
                         }
                    />
                    <Input
                         label="Total Weight"
                         value={cuttingData.totalWeight}
                         onChange={(value: string) =>
                              setCuttingData({
                                   ...cuttingData,
                                   totalWeight: +value,
                              })
                         }
                    />
                    <div className="p-2 py-3 bg-gray-50">
                         <p>Select Sizes</p>
                    </div>
                    {sizeArray.map((size) => (
                         <Input
                              label={`${size}`}
                              type="number"
                              value={
                                   cuttingData.cuttingSizes.find(
                                        (e) => e.size === size
                                   )?.quantity || 0
                              }
                              onChange={(value: string) => {
                                   const cuttingSizes =
                                        cuttingData.cuttingSizes;
                                   cuttingSizes[
                                        cuttingSizes.findIndex(
                                             (e) => e.size === size
                                        )
                                   ] = {
                                        size,
                                        quantity: +value,
                                   };
                                   setCuttingData({
                                        ...cuttingData,
                                        cuttingSizes,
                                   });
                              }}
                         />
                    ))}

                    <Button
                         buttonText="Add"
                         onClick={async () => {
                              await createCutting({
                                   variables: cuttingData,
                              });
                              hideBoxFunction();
                         }}
                    />
               </div>
          </PopUp>
     );
};
