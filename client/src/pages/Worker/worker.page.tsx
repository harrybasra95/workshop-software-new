import { useMutation, useQuery } from '@apollo/client';
import { GET_ALL_WORKERS, CREATE_WORKER } from 'apis/worker.api';
import Button from 'components/Button';
import PopUp from 'components/PopUp';
import OuterLayout from 'layouts/Outer';
import { useState } from 'react';
import { WorkerI } from 'utils/interfaces/worker.interface';

export const Worker = () => {
     const [isPopUpVisible, setisPopUpVisible] = useState(false);
     const { data } = useQuery<{ workers: WorkerI[] }>(GET_ALL_WORKERS);
     return (
          <OuterLayout
               plusIconClick={() => {
                    setisPopUpVisible(true);
               }}
          >
               <div>
                    <WorkerPopUp
                         isVisible={isPopUpVisible}
                         hideBoxFunction={() => {
                              setisPopUpVisible(false);
                         }}
                    />
                    <h1 className="text-2xl">Workers</h1>
                    <div>
                         <table className="table-auto mt-5">
                              <thead>
                                   <th className="border border-black">Name</th>
                              </thead>
                              <tbody>
                                   {data &&
                                        data.workers.map(({ name }) => (
                                             <tr>
                                                  <td className="p-2 border border-black">
                                                       {name}
                                                  </td>
                                             </tr>
                                        ))}
                              </tbody>
                         </table>
                    </div>
               </div>
          </OuterLayout>
     );
};

export const WorkerPopUp: React.FunctionComponent<{
     isVisible: boolean;
     hideBoxFunction: Function;
}> = ({ isVisible, hideBoxFunction }) => {
     const [workerName, setWorkerName] = useState('Tipu');
     const [createWorker] = useMutation(CREATE_WORKER);
     return (
          <PopUp
               title="Add Worker"
               isVisible={isVisible}
               hideBoxFunction={hideBoxFunction}
          >
               <div className="flex flex-col align-middle justify-center pt-5">
                    <input
                         className="border-solid border-grey border p-2"
                         type="text"
                         value={workerName}
                         onChange={({ target: { value } }) =>
                              setWorkerName(value)
                         }
                         placeholder="Name"
                         name="name"
                    />
                    <Button
                         buttonText="Add"
                         onClick={async () => {
                              await createWorker({
                                   variables: { name: workerName },
                              });
                              hideBoxFunction();
                         }}
                    />
               </div>
          </PopUp>
     );
};
