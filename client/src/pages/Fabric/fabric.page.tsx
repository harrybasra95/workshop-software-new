import { useMutation, useQuery } from '@apollo/client';
import { CREATE_FABRIC, GET_ALL_FABRICS } from 'apis/fabric.api';
import Button from 'components/Button';
import Input from 'components/Input';
import PopUp from 'components/PopUp';
import Select from 'components/Select';
import OuterLayout from 'layouts/Outer';
import { useState } from 'react';
import { CreateFabricDTO, FabricI } from 'utils/interfaces/fabric.interface';

export const Fabric = () => {
     const [isPopUpVisible, setisPopUpVisible] = useState(false);
     const { data } = useQuery<{ fabrics: FabricI[] }>(GET_ALL_FABRICS);
     return (
          <OuterLayout
               plusIconClick={() => {
                    setisPopUpVisible(true);
               }}
          >
               <div>
                    <FabricPopUp
                         isVisible={isPopUpVisible}
                         hideBoxFunction={() => {
                              setisPopUpVisible(false);
                         }}
                    />
                    <h1 className="text-2xl">Fabrics</h1>
                    <div>
                         <table className="table-auto mt-5">
                              <thead>
                                   <th className="border p-2 border-black">
                                        Name
                                   </th>
                                   <th className="border p-2 border-black">
                                        GSM
                                   </th>
                                   <th className="border p-2 border-black">
                                        Length
                                   </th>
                                   <th className="border p-2 border-black">
                                        Width Type
                                   </th>
                                   <th className="border p-2 border-black">
                                        Weight
                                   </th>
                              </thead>
                              <tbody>
                                   {data &&
                                        data.fabrics.map(
                                             ({
                                                  name,
                                                  gsm,
                                                  length,
                                                  weight,
                                                  widthType,
                                             }) => (
                                                  <tr>
                                                       <td className="p-2 border border-black">
                                                            {name}
                                                       </td>
                                                       <td className="p-2 border border-black">
                                                            {gsm}
                                                       </td>
                                                       <td className="p-2 border border-black">
                                                            {length}
                                                       </td>
                                                       <td className="p-2 border border-black">
                                                            {widthType}
                                                       </td>
                                                       <td className="p-2 border border-black">
                                                            {weight}
                                                       </td>
                                                  </tr>
                                             )
                                        )}
                              </tbody>
                         </table>
                    </div>
               </div>
          </OuterLayout>
     );
};

export const FabricPopUp: React.FunctionComponent<{
     isVisible: boolean;
     hideBoxFunction: Function;
}> = ({ isVisible, hideBoxFunction }) => {
     const [fabricData, setFabricData] = useState<CreateFabricDTO>({
          name: '',
          gsm: 0,
          weight: 0,
          length: 0,
          widthType: 'open',
     });
     const [createFabric] = useMutation(CREATE_FABRIC);
     return (
          <PopUp
               title="Add Fabric"
               isVisible={isVisible}
               hideBoxFunction={hideBoxFunction}
          >
               <div className="flex flex-col align-middle justify-center pt-5">
                    <Input
                         label="Name"
                         value={fabricData.name}
                         onChange={(value: string) =>
                              setFabricData({ ...fabricData, name: value })
                         }
                    />
                    <Input
                         label="Gsm"
                         value={fabricData.gsm}
                         onChange={(value: string) =>
                              setFabricData({ ...fabricData, gsm: +value })
                         }
                    />
                    <Input
                         label="Weight"
                         value={fabricData.weight}
                         onChange={(value: string) =>
                              setFabricData({ ...fabricData, weight: +value })
                         }
                    />
                    <Input
                         label="Length"
                         value={fabricData.length}
                         onChange={(value: string) =>
                              setFabricData({ ...fabricData, length: +value })
                         }
                    />
                    <Select
                         label="Width type"
                         value={fabricData.widthType}
                         onChange={(value: any) =>
                              setFabricData({ ...fabricData, widthType: value })
                         }
                         options={[
                              { value: 'tube', name: 'Tube' },
                              { value: 'open', name: 'Open' },
                         ]}
                    />

                    <Button
                         buttonText="Add"
                         onClick={async () => {
                              await createFabric({
                                   variables: fabricData,
                              });
                              hideBoxFunction();
                         }}
                    />
               </div>
          </PopUp>
     );
};
