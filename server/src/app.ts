import express from 'express';
import { buildSchema } from 'type-graphql';
import { ApolloServer } from 'apollo-server-express';
import { MikroORM } from '@mikro-orm/core';
import ormConfig from './orm.config';
import customAuthChecker from 'utils/helpers/authChecker';
import UserResolver from '@resolvers/user.resolver';
import FabricResolver from '@resolvers/fabric.resolver';
import WorkerResolver from '@resolvers/worker.resolver';
import CuttingResolver from '@resolvers/cutting.resolver';

class App {
     private _app: express.Application;

     set app(app: express.Application) {
          this._app = app;
     }

     get app(): express.Application {
          return this._app;
     }

     private apolloServer: any;
     private orm: any;

     constructor() {
          this.app = express();
          (async () => {
               this.orm = await MikroORM.init(ormConfig);
               const migrator = this.orm.getMigrator();
               const migrations = await migrator.getPendingMigrations();
               if (migrations && migrations.length > 0) {
                    await migrator.up();
               }
               try {
                    const schema = await buildSchema({
                         resolvers: [
                              UserResolver,
                              FabricResolver,
                              WorkerResolver,
                              CuttingResolver,
                         ],
                         validate: true,
                         authChecker: customAuthChecker,
                    });
                    this.apolloServer = new ApolloServer({
                         schema,
                         uploads: false,
                         context: ({ req }: any) => ({
                              req,
                              em: this.orm.em.fork(),
                         }),
                    });
                    this.config();
               } catch (error) {
                    console.log(error);
               }
          })();
     }

     private config(): void {
          this.app.use('/media', express.static('../media'));
          this.apolloServer.applyMiddleware({
               app: this.app,
               path: '/graphql',
          });
     }
}

export default new App().app;
