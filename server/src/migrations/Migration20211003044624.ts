import { Migration } from '@mikro-orm/migrations';

export class Migration20211003044624 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table `users` modify `role` varchar(255) default \'user\';');

    this.addSql('create table `cuttings` (`id` varchar(255) not null, `createdAt` datetime not null, `updatedAt` datetime not null, `date` datetime null, `layerLength` int(11) null, `layerWeight` int(11) null, `totalWeight` int(11) null, `notes` varchar(255) null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `cuttings` add primary key `cuttings_pkey`(`id`);');
  }

}
