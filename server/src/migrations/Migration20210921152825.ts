import { Migration } from '@mikro-orm/migrations';

export class Migration20210921152825 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `workers` (`id` varchar(255) not null, `createdAt` datetime not null, `updatedAt` datetime not null, `name` varchar(255) null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `workers` add primary key `workers_pkey`(`id`);');

    this.addSql('alter table `users` modify `role` varchar(255) not null default \'user\';');
  }

}
