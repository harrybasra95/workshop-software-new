import { Migration } from '@mikro-orm/migrations';

export class Migration20210829165924 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table `users` modify `role` varchar(255) default \'user\';');

    this.addSql('create table `fabrics` (`id` varchar(255) not null, `createdAt` datetime not null, `updatedAt` datetime not null, `name` varchar(255) null, `gsm` int(11) null, `weight` int(11) null, `length` int(11) null, `diameter` int(11) null, `color` varchar(255) null, `widthType` varchar(255) not null, `rate` float null, `transportation` float null, `cashAmount` float null, `chequeAmount` float null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `fabrics` add primary key `fabrics_pkey`(`id`);');
  }

}
