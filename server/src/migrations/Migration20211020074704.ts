import { Migration } from '@mikro-orm/migrations';

export class Migration20211020074704 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table `users` modify `role` varchar(255) not null default \'user\';');

    this.addSql('alter table `cuttingSizes` add `cuttingId` varchar(255) not null;');
    this.addSql('alter table `cuttingSizes` add index `cuttingSizes_cuttingId_index`(`cuttingId`);');

    this.addSql('alter table `cuttingSizes` add constraint `cuttingSizes_cuttingId_foreign` foreign key (`cuttingId`) references `cuttings` (`id`) on update cascade;');
  }

}
