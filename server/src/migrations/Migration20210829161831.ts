import { Migration } from '@mikro-orm/migrations';

export class Migration20210829161831 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `users` (`id` varchar(255) not null, `createdAt` datetime not null, `updatedAt` datetime not null, `email` varchar(255) null, `name` varchar(255) null, `role` varchar(255) not null default \'user\', `password` varchar(255) null, `token` varchar(255) null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `users` add primary key `users_pkey`(`id`);');
  }

}
