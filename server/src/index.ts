import { createServer } from 'http';
import app from './app';
import config from './config/config';
const PORT = config.PORT;

const httpServer = createServer(app);

httpServer.listen({ port: PORT }, (): void =>
     console.log('Server started at ', PORT)
);
