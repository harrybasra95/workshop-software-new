import Cutting from '@entities/cutting.entity';
import CuttingSize from '@entities/cuttingSize.entity';
import Fabric from '@entities/fabric.entity';
import MyContext from '@interfaces/context.interface';
import CuttingValidator, {
     CuttingValidatorUpdate,
} from '@validators/cutting.validator';
import { GraphQLResolveInfo } from 'graphql';
import fieldsToRelations from 'graphql-fields-to-relations';
import { Arg, Ctx, Info, Mutation, Query, Resolver } from 'type-graphql';

@Resolver(Cutting)
export default class CuttingResolver {
     @Query((_returns) => [Cutting], {
          nullable: true,
          description: 'Get details of all the Cuttings',
     })
     async cuttings(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo
     ): Promise<Cutting[]> {
          const relationPaths = fieldsToRelations(info);
          const foundCuttings = await ctx.em
               .getRepository(Cutting)
               .findAll(relationPaths);

          return foundCuttings as Cutting[];
     }

     @Query((_returns) => Cutting, {
          description: 'Get cutting by cutting id',
     })
     async cuttingById(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo,

          @Arg('id', (_type) => String)
          id: string
     ): Promise<Cutting> {
          const relationPaths = fieldsToRelations(info);
          const foundCutting = await ctx.em
               .getRepository(Cutting)
               .findOne(id, relationPaths);
          return foundCutting as Cutting;
     }

     @Mutation((_returns) => Cutting, {
          description: 'Create cutting',
     })
     async createCutting(
          @Ctx()
          ctx: MyContext,

          @Arg('data', (_type) => CuttingValidator)
          data: CuttingValidator
     ): Promise<Cutting> {
          const cuttingRepository = ctx.em.getRepository(Cutting);
          const cuttingSizeRepository = ctx.em.getRepository(CuttingSize);
          const fabricRepository = ctx.em.getRepository(Fabric);

          const fabric = await fabricRepository.findOne(data.fabricId);
          if (!fabric) throw 'Fabric not found';

          const cutting = new Cutting(data);
          cutting.fabric = fabric;
          await cuttingRepository.persist(cutting).flush();

          const cuttingSizes = data.cuttingSizes.map((cuttingSizeData) => {
               const cuttingSize = new CuttingSize(cuttingSizeData);
               cuttingSize.cutting = cutting;
               return cuttingSize;
          });
          await cuttingSizeRepository.persist(cuttingSizes).flush();

          return cutting;
     }

     @Mutation((_returns) => Cutting, {
          description: 'Update cutting details',
     })
     async updateCutting(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo,

          @Arg('data', (_type) => CuttingValidatorUpdate)
          data: CuttingValidatorUpdate
     ): Promise<Cutting> {
          const relationPaths = fieldsToRelations(info);
          const cuttingRepository = ctx.em.getRepository(Cutting);
          const foundCutting = await cuttingRepository.findOne(
               data.id,
               relationPaths
          );
          if (!foundCutting) throw 'Cutting not found';
          foundCutting.assign(data);
          await cuttingRepository.persist(foundCutting).flush();
          return foundCutting as Cutting;
     }

     @Mutation((_returns) => String, {
          description: 'Delete cutting',
     })
     async deleteCutting(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo,

          @Arg('id', (_type) => String)
          id: string
     ): Promise<String> {
          const relationPaths = fieldsToRelations(info);
          const cuttingRepository = ctx.em.getRepository(Cutting);
          const foundCutting = await cuttingRepository.findOne(
               id,
               relationPaths
          );
          if (!foundCutting) throw 'Cutting not found';
          await cuttingRepository.removeAndFlush(foundCutting);
          return 'Cutting removed successfully';
     }
}
