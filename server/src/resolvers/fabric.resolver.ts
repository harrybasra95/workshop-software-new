import Fabric from '@entities/fabric.entity';
import MyContext from '@interfaces/context.interface';
import FabricValidator, {
     FabricValidatorUpdate,
} from '@validators/fabric.validator';
import { GraphQLResolveInfo } from 'graphql';
import fieldsToRelations from 'graphql-fields-to-relations';
import { Arg, Ctx, Info, Mutation, Query, Resolver } from 'type-graphql';

@Resolver(Fabric)
export default class FabricResolver {
     @Query((_returns) => [Fabric], {
          nullable: true,
          description: 'Get details of all the Fabrics',
     })
     async fabrics(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo
     ): Promise<[Fabric]> {
          const relationPaths = fieldsToRelations(info);
          const foundFabrics = await ctx.em
               .getRepository(Fabric)
               .findAll(relationPaths);

          return foundFabrics as [Fabric];
     }

     @Query((_returns) => Fabric, {
          description: 'Get fabric by fabric id',
     })
     async fabricById(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo,

          @Arg('id', (_type) => String)
          id: string
     ): Promise<Fabric> {
          const relationPaths = fieldsToRelations(info);
          const foundFabric = await ctx.em
               .getRepository(Fabric)
               .findOne(id, relationPaths);
          return foundFabric as Fabric;
     }

     @Mutation((_returns) => Fabric, {
          description: 'Create fabric',
     })
     async createFabric(
          @Ctx()
          ctx: MyContext,

          @Arg('data', (_type) => FabricValidator)
          data: FabricValidator
     ): Promise<Fabric> {
          const fabricRepository = ctx.em.getRepository(Fabric);
          const { name } = data;
          let fabric;

          fabric = await fabricRepository.findOne({ name });
          if (fabric) throw 'Fabric already exists with this name';
          fabric = new Fabric(data);
          await fabricRepository.persist(fabric).flush();

          fabric.assign(fabric);
          await fabricRepository.persist(fabric).flush();
          return fabric;
     }

     @Mutation((_returns) => Fabric, {
          description: 'Update fabric details',
     })
     async updateFabric(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo,

          @Arg('data', (_type) => FabricValidatorUpdate)
          data: FabricValidatorUpdate
     ): Promise<Fabric> {
          const relationPaths = fieldsToRelations(info);
          const fabricRepository = ctx.em.getRepository(Fabric);
          const foundFabric = await fabricRepository.findOne(
               data.id,
               relationPaths
          );
          if (!foundFabric) throw 'Fabric not found';
          foundFabric.assign(data);
          await fabricRepository.persist(foundFabric).flush();
          return foundFabric as Fabric;
     }

     @Mutation((_returns) => String, {
          description: 'Delete fabric',
     })
     async deleteFabric(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo,

          @Arg('id', (_type) => String)
          id: string
     ): Promise<String> {
          const relationPaths = fieldsToRelations(info);
          const fabricRepository = ctx.em.getRepository(Fabric);
          const foundFabric = await fabricRepository.findOne(id, relationPaths);
          if (!foundFabric) throw 'Fabric not found';
          await fabricRepository.removeAndFlush(foundFabric);
          return 'Fabric removed successfully';
     }
}
