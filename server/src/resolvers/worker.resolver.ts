import Worker from '@entities/worker.entity';
import MyContext from '@interfaces/context.interface';
import WorkerValidator, {
     WorkerValidatorUpdate,
} from '@validators/worker.validator';
import { GraphQLResolveInfo } from 'graphql';
import fieldsToRelations from 'graphql-fields-to-relations';
import { Arg, Ctx, Info, Mutation, Query, Resolver } from 'type-graphql';

@Resolver(Worker)
export default class WorkerResolver {
     @Query((_returns) => [Worker], {
          nullable: true,
          description: 'Get details of all the Workers',
     })
     async workers(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo
     ): Promise<[Worker]> {
          const relationPaths = fieldsToRelations(info);
          const foundWorkers = await ctx.em
               .getRepository(Worker)
               .findAll(relationPaths);

          return foundWorkers as [Worker];
     }

     @Query((_returns) => Worker, {
          description: 'Get worker by worker id',
     })
     async workerById(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo,

          @Arg('id', (_type) => String)
          id: string
     ): Promise<Worker> {
          const relationPaths = fieldsToRelations(info);
          const foundWorker = await ctx.em
               .getRepository(Worker)
               .findOne(id, relationPaths);
          return foundWorker as Worker;
     }

     @Mutation((_returns) => Worker, {
          description: 'Create worker',
     })
     async createWorker(
          @Ctx()
          ctx: MyContext,

          @Arg('data', (_type) => WorkerValidator)
          data: WorkerValidator
     ): Promise<Worker> {
          const workerRepository = ctx.em.getRepository(Worker);
          const { name } = data;
          let worker;

          worker = await workerRepository.findOne({ name });
          if (worker) throw 'Worker already exists with this name';
          worker = new Worker(data);
          await workerRepository.persist(worker).flush();

          worker.assign(worker);
          await workerRepository.persist(worker).flush();
          return worker;
     }

     @Mutation((_returns) => Worker, {
          description: 'Update worker details',
     })
     async updateWorker(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo,

          @Arg('data', (_type) => WorkerValidatorUpdate)
          data: WorkerValidatorUpdate
     ): Promise<Worker> {
          const relationPaths = fieldsToRelations(info);
          const workerRepository = ctx.em.getRepository(Worker);
          const foundWorker = await workerRepository.findOne(
               data.id,
               relationPaths
          );
          if (!foundWorker) throw 'Worker not found';
          foundWorker.assign(data);
          await workerRepository.persist(foundWorker).flush();
          return foundWorker as Worker;
     }

     @Mutation((_returns) => String, {
          description: 'Delete worker',
     })
     async deleteWorker(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo,

          @Arg('id', (_type) => String)
          id: string
     ): Promise<String> {
          const relationPaths = fieldsToRelations(info);
          const workerRepository = ctx.em.getRepository(Worker);
          const foundWorker = await workerRepository.findOne(id, relationPaths);
          if (!foundWorker) throw 'Worker not found';
          await workerRepository.removeAndFlush(foundWorker);
          return 'Worker removed successfully';
     }
}
