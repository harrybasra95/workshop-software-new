import User from '@entities/user.entity';
import AuthUser from '@entities/auth.entity';
import MyContext from '@interfaces/context.interface';
import UserValidator, { UserValidatorUpdate } from '@validators/user.validator';
import { GraphQLResolveInfo } from 'graphql';
import fieldsToRelations from 'graphql-fields-to-relations';
import { Arg, Ctx, Info, Mutation, Query, Resolver } from 'type-graphql';
import { createToken } from 'utils/helpers/token';

@Resolver(User)
export default class UserResolver {
     @Query((_returns) => [User], {
          nullable: true,
          description: 'Get details of all the Users',
     })
     async users(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo
     ): Promise<[User]> {
          const relationPaths = fieldsToRelations(info);
          const foundUsers = await ctx.em
               .getRepository(User)
               .findAll(relationPaths);

          return foundUsers as [User];
     }

     @Query((_returns) => User, {
          description: 'Get user by user id',
     })
     async userById(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo,

          @Arg('id', (_type) => String)
          id: string
     ): Promise<User> {
          const relationPaths = fieldsToRelations(info);
          const foundUser = await ctx.em
               .getRepository(User)
               .findOne(id, relationPaths);
          return foundUser as User;
     }

     @Mutation((_returns) => AuthUser, {
          description: 'Create user',
     })
     async createUser(
          @Ctx()
          ctx: MyContext,

          @Arg('data', (_type) => UserValidator)
          data: UserValidator
     ): Promise<AuthUser> {
          const userRepository = ctx.em.getRepository(User);
          const { email } = data;
          let user;

          user = await userRepository.findOne({ email });
          if (user) throw 'User already exists with this email';
          user = new User(data);
          await userRepository.persist(user).flush();

          user.token = createToken(user.id);
          user.assign(user);
          await userRepository.persist(user).flush();
          return { user, token: user.token } as AuthUser;
     }

     @Mutation((_returns) => User, {
          description: 'Update user details',
     })
     async updateUser(
          @Ctx()
          ctx: MyContext,

          @Info()
          info: GraphQLResolveInfo,

          @Arg('data', (_type) => UserValidatorUpdate)
          data: UserValidatorUpdate
     ): Promise<User> {
          const relationPaths = fieldsToRelations(info);
          const userRepository = ctx.em.getRepository(User);
          const foundUser = await userRepository.findOne(
               data.id,
               relationPaths
          );
          if (!foundUser) throw 'User not found';
          foundUser.assign(data);
          await userRepository.persist(foundUser).flush();
          return foundUser as User;
     }

     @Mutation((_returns) => User, {
          description: 'Change password for user',
     })
     async changePassword(
          @Ctx()
          ctx: MyContext,

          @Arg('newPassword', (_type) => String)
          newPassword: string,

          @Arg('userId', (_type) => String)
          userId: string
     ): Promise<User> {
          const foundUser = await ctx.em.getRepository(User).findOne(userId);
          if (!foundUser) throw 'User not found';
          foundUser.password = newPassword;
          await ctx.em.getRepository(User).persist(foundUser).flush();

          return foundUser as User;
     }
}
