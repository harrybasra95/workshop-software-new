import { IsNumber, IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export default class FabricValidator {
     @Field({ nullable: true })
     @IsString()
     name: String;

     @Field({ nullable: true })
     @IsNumber()
     gsm: Number;

     @Field({ nullable: true })
     @IsNumber()
     weight: Number;

     @Field({ nullable: true })
     @IsNumber()
     length: Number;

     @Field({ nullable: true })
     @IsNumber()
     diameter: Number;

     @Field({ nullable: true })
     @IsString()
     color: String;

     @Field({ nullable: true })
     @IsString()
     widthType: String;

     @Field({ nullable: true })
     @IsNumber()
     rate: Number;

     @Field({ nullable: true })
     @IsNumber()
     transportation: Number;

     @Field({ nullable: true })
     @IsNumber()
     cashAmount: Number;

     @Field({ nullable: true })
     @IsNumber()
     chequeAmount: Number;
}

@InputType()
export class FabricValidatorUpdate extends FabricValidator {
     @Field()
     @IsString()
     id: string;
}
