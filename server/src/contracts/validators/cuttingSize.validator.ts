import { IsNumber, IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export default class CuttingSizeValidator {
     @Field({ nullable: true })
     @IsNumber()
     quantity: Number;

     @Field({ nullable: true })
     @IsNumber()
     size: Number;
}

@InputType()
export class CuttingSizeValidatorUpdate extends CuttingSizeValidator {
     @Field()
     @IsString()
     id: string;
}
