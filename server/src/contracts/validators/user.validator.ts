import { Roles } from '@enums/role';
import {
     Equals,
     IsDefined,
     IsEmail,
     IsString,
     MinLength,
     ValidateIf,
} from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export default class UserValidator {
     @Field({ nullable: true })
     @IsEmail()
     email: String;

     @Field({ nullable: true })
     @IsString()
     name: String;

     @Field((_type) => Roles)
     @IsString()
     role: Roles;

     @Field({ nullable: true })
     @IsString()
     status: String;

     @Field({ nullable: true })
     @ValidateIf((u) => u.loginType === 'email')
     @IsDefined()
     @MinLength(8)
     @IsString()
     password: String;
}

@InputType()
export class UserValidatorUpdate extends UserValidator {
     @Field()
     @IsString()
     id: string;

     @Field({ nullable: true })
     @Equals(undefined)
     email: String;

     @Field((_type) => Roles, { nullable: true })
     @Equals(undefined)
     role: Roles;
}
