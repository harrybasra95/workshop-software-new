import { IsDate, IsNumber, IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';
import CuttingSizeValidator from './cuttingSize.validator';

@InputType()
export default class CuttingValidator {
     @Field({ nullable: true })
     @IsDate()
     date: Date;

     @Field({ nullable: true })
     @IsNumber()
     layerLength: Number;

     @Field({ nullable: true })
     @IsNumber()
     layerWeight: Number;

     @Field({ nullable: true })
     @IsNumber()
     totalWeight: Number;

     @Field({ nullable: true })
     @IsString()
     notes: String;

     @Field({ nullable: true })
     @IsString()
     fabricId: String;

     @Field((_type) => [CuttingSizeValidator], { nullable: true })
     cuttingSizes: CuttingSizeValidator[];
}

@InputType()
export class CuttingValidatorUpdate extends CuttingValidator {
     @Field()
     @IsString()
     id: string;
}
