import { IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export default class WorkerValidator {
     @Field({ nullable: true })
     @IsString()
     name: String;
}

@InputType()
export class WorkerValidatorUpdate extends WorkerValidator {
     @Field()
     @IsString()
     id: string;
}
