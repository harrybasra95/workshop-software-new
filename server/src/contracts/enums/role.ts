import { registerEnumType } from 'type-graphql';

export enum Roles {
     ADMIN = 'admin',
     USER = 'user',
}

registerEnumType(Roles, {
     name: 'Roles',
     description: 'User Roles',
});
