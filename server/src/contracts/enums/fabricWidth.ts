import { registerEnumType } from 'type-graphql';

export enum FabricWidth {
     OPEN = 'open',
     TUBE = 'tube',
}

registerEnumType(FabricWidth, {
     name: 'FabricWidth',
     description: 'Fabric Width Type',
});
