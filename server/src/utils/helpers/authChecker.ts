import MyContext from '@interfaces/context.interface';
import TokenI from '@interfaces/token.interface';
import { AuthChecker } from 'type-graphql';
import { verifyToken } from './token';

const customAuthChecker: AuthChecker<MyContext> = async ({ context }) => {
     const tokenName = 'x-access-token';
     const token = context.req.headers[tokenName]?.toString();
     if (!token) throw new Error('Token is required');
     const { id, model } = verifyToken(token) as TokenI;

     if (!id || !model) throw Error('Invalid token');
     if (model === 'user') {
          // const foundUser: User | undefined = await User.findOne({ id });
          // if (!foundUser) throw Error('Invalid Token');
          // context.req.user = foundUser;
          // context.req.model = model;
     }

     return true; // or false if access is denied
};

export default customAuthChecker;
