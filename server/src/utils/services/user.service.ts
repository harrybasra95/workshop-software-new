import User from '@entities/user.entity';
import bcrypt from 'bcrypt';

export const sendOTPOnEmail = (user: User, otp: Number) => {
     console.log('OTP Service pending');
     console.log(user.email, otp);
};

export const generateOTP = (length = 5): Number => {
     return Math.floor(Math.random() * 9000) + Math.pow(10, length - 1);
};

export const comparePassword = (password: string, hash: string): Boolean => {
     return bcrypt.compareSync(password, hash);
};
