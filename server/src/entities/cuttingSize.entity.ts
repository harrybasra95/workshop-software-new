import Base from '@baseEntity';
import { Entity, ManyToOne, Property } from '@mikro-orm/core';
import CuttingSizeValidator from '@validators/cuttingSize.validator';
import { Field, ObjectType } from 'type-graphql';
import Cutting from './cutting.entity';

@Entity({ tableName: 'cuttingSizes' })
@ObjectType({ description: 'The Cutting Size model' })
export default class CuttingSize extends Base<CuttingSize> {
     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     quantity: Number;

     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     size: Number;

     @Field((_type) => Cutting)
     @ManyToOne(() => Cutting, { joinColumn: 'cuttingId' })
     cutting!: Cutting;

     constructor(body: CuttingSizeValidator) {
          super(body);
     }
}
