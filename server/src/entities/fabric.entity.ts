import Base from '@baseEntity';
import { FabricWidth } from '@enums/fabricWidth';
import { Entity, Property } from '@mikro-orm/core';
import FabricValidator from '@validators/fabric.validator';
import { Field, Float, ObjectType } from 'type-graphql';

@Entity({ tableName: 'fabrics' })
@ObjectType({ description: 'The Fabric model' })
export default class Fabric extends Base<Fabric> {
     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     name: String;

     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     gsm: Number;

     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     weight: Number;

     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     length: Number;

     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     diameter: Number;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     color: String;

     @Field((_type) => FabricWidth, { nullable: true })
     @Property()
     widthType: String;

     @Field((_type) => Float, { nullable: true })
     @Property({ nullable: true, type: 'float' })
     rate: Number;

     @Field((_type) => Float, { nullable: true })
     @Property({ nullable: true, type: 'float' })
     transportation: Number;

     @Field((_type) => Float, { nullable: true })
     @Property({ nullable: true, type: 'float' })
     cashAmount: Number;

     @Field((_type) => Float, { nullable: true })
     @Property({ nullable: true, type: 'float' })
     chequeAmount: Number;

     constructor(body: FabricValidator) {
          super(body);
     }
}
