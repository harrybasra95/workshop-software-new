import Base from '@baseEntity';
import {
     Collection,
     Entity,
     ManyToOne,
     OneToMany,
     Property,
} from '@mikro-orm/core';
import CuttingValidator from '@validators/cutting.validator';
import { Field, ObjectType } from 'type-graphql';
import CuttingSize from './cuttingSize.entity';
import Fabric from './fabric.entity';

@Entity({ tableName: 'cuttings' })
@ObjectType({ description: 'The Cutting model' })
export default class Cutting extends Base<Cutting> {
     @Field((_type) => Date, { nullable: true })
     @Property({ nullable: true })
     date: Date;

     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     layerLength: Number;

     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     layerWeight: Number;

     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     totalWeight: Number;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     notes: String;

     @Field((_type) => Fabric)
     @ManyToOne({ joinColumn: 'fabricId' })
     fabric!: Fabric;

     @Field((_type) => [CuttingSize])
     @OneToMany(() => CuttingSize, (cuttingSize) => cuttingSize.cutting)
     cuttingSizes = new Collection<CuttingSize>(this);

     constructor(body: CuttingValidator) {
          super(body);
     }
}
