import Base from '@baseEntity';
import { Entity, Property } from '@mikro-orm/core';
import WorkerValidator from '@validators/worker.validator';
import { Field, ObjectType } from 'type-graphql';

@Entity({ tableName: 'workers' })
@ObjectType({ description: 'The Worker model' })
export default class Worker extends Base<Worker> {
     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     name: String;

     constructor(body: WorkerValidator) {
          super(body);
     }
}
