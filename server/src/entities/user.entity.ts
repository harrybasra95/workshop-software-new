import Base from '@baseEntity';
import { Roles } from '@enums/role';
import { BeforeCreate, BeforeUpdate, Entity, Property } from '@mikro-orm/core';
import UserValidator from '@validators/user.validator';
import { hashSync } from 'bcrypt';
import { Field, ObjectType } from 'type-graphql';

@Entity({ tableName: 'users' })
@ObjectType({ description: 'The User model' })
export default class User extends Base<User> {
     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     email: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     name: String;

     @Field((_type) => Roles, { nullable: true })
     @Property({ default: Roles.USER })
     role: String;

     @Property({ nullable: true })
     password: string;

     @Property({ nullable: true })
     token: String;

     @BeforeUpdate()
     @BeforeCreate()
     setPassword() {
          if (this.password) {
               this.password = hashSync(this.password as string, 10);
          }
     }

     constructor(body: UserValidator) {
          super(body);
     }
}
